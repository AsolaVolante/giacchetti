package Interfaccie;
// � un interfacia: non la implemento, ma la definisco 
public interface A {
	// non posso dichiarare variabili d' istanza, solo costanti
	//metodi astratti
	static int z = 0;
//	static � sottointeso
	int i = 3;
	abstract void a();
//	abstract � sottointeso
	void b();
}