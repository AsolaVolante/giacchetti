package sercizietti;

public class Demo {
//polimorfismo, una interfaccia, molti metodi!

	public static void main(String[] args) {
		FiguraPiana figuraPiana = null;
		int n =(int)(Math.random()*3);
		
		switch(n) {
		case 0: figuraPiana = new Cerchio(2); break;
		case 1: figuraPiana = new Triangolo(2,2); break;
		case 2: figuraPiana = new Rettangolo(2,2); break;
		}
		
		float area = figuraPiana.area();
		System.out.print(area);
	}

}
