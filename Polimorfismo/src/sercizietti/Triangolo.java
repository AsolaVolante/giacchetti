package sercizietti;

public class Triangolo extends FiguraPiana {
	
	float base,altezza;

	public Triangolo(float base, float altezza) {
		this.base = base;
		this.altezza = altezza;
	}

	@Override
	public float area() {
		return (base*altezza)/2;
	}

}
