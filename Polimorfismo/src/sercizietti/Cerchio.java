package sercizietti;

public class Cerchio extends FiguraPiana {

	float raggio;

	public Cerchio(float raggio) {
		this.raggio = raggio;
	}

	@Override
	public float area() {
		return (float) (Math.PI*raggio*raggio);
	}

}
